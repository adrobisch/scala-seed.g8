import com.typesafe.sbt.packager.docker._

val scalaV = "$scala_version$"
val circeVersion = "0.8.0"
val kamonVersion = "0.6.7"
val akkaHttpVersion = "10.0.7"

lazy val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "$scalatest_version$" % Test,
  "org.mockito" % "mockito-core" % "2.7.22" % Test
)

lazy val loggingDependencies = Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
  "ch.qos.logback" % "logback-classic" % "1.1.7"
)

lazy val commonSettings = Seq(
  organization := "$organisation$",
  scalaVersion := scalaV,
  version := "0.1.0-SNAPSHOT",

  libraryDependencies ++= testDependencies ++ loggingDependencies,
  wartremoverWarnings ++= Warts.allBut(Wart.Any, Wart.NonUnitStatements),
  coverageMinimum := 30,
  coverageFailOnMinimum := true  
)

lazy val dockerSettings = Seq(
  dockerBaseImage := "openjdk:8u131-jdk",
  version in Docker := git.formattedShaVersion.value.getOrElse("latest"),
  dockerUpdateLatest := true,
  dockerExposedPorts := Seq(8080),
  dockerCommands += Cmd("ENV", "FISH", "42")
)

lazy val core = (project in file("core")).
  settings(commonSettings).
  settings(
    name := "$name$-core"
  )

lazy val server = (project in file("server")).
  enablePlugins(JavaAppPackaging, DockerPlugin, GitVersioning).
  settings(commonSettings).
  settings(dockerSettings).
  settings(
    name := "$name$-server",

    libraryDependencies ++= Seq(
      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-generic-extras" % circeVersion,
      "io.circe" %% "circe-literal" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion,

      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "de.heikoseeberger" %% "akka-http-circe" % "1.17.0",
      "com.typesafe" % "config" % "1.3.1",

      "io.kamon" %% "kamon-core" % kamonVersion,
      "io.kamon" %% "kamon-statsd" % kamonVersion,
      "io.kamon" %% "kamon-akka-2.4" % kamonVersion,
      "io.kamon" %% "kamon-akka-http" % kamonVersion,
      "io.kamon" %% "kamon-system-metrics" % kamonVersion,

      "com.github.swagger-akka-http" %% "swagger-akka-http" % "0.9.2",
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test
    )
  )

lazy val root = (project in file(".")).
  settings(commonSettings).
  settings(
    name := "$name$",
    test := {},
    publishLocal := {}, // root project should not publish itself
    publish := {}
  ).aggregate(core, server)

addCommandAlias("testWithCoverage", ";clean;coverage;test;coverageReport")