package $package$

object Hello extends GreetingApp

trait Greeting {
  lazy val greeting: String = "hello"
}

trait GreetingApp extends App with Greeting {
  println(greeting)
}
