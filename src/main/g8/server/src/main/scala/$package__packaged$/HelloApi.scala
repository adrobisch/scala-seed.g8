package $package$

import javax.ws.rs.Path

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import io.swagger.annotations._

final case class GreetingResponse(greeting: String)

trait HelloApi {
    @Api(value = "/hello", produces = "text/plain")
    @Path("/hello")
    @ApiOperation(value = "show a greeting", httpMethod = "GET")
    @ApiImplicitParams(Array(
        new ApiImplicitParam(name = "name", value = "name to include in the greeting", dataType = "string", paramType = "query", required = true)
    ))
    @ApiResponses(Array(
        new ApiResponse(code = 200, message = "Successfully showed the greeting", response = classOf[GreetingResponse]),
        new ApiResponse(code = 400, message = "Parameters missing")
    ))
    def route: Route = path("hello") { 
        get {
            parameterMap { params =>
                complete("hello")
            }
        }
    }
}