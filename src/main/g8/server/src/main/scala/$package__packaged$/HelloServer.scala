package $package$

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging
import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration._
import scala.util.{ Failure, Success }

import kamon.Kamon

trait HelloServer extends LazyLogging { self: HelloRoutes =>
    implicit val system = ActorSystem("http-system")
    implicit val materializer = ActorMaterializer()

    implicit val executionContext = system.dispatcher

    def start = {
        Kamon.start()

        val bindAndHandle = Http().bindAndHandle(routes, "localhost", 8080)

        bindAndHandle.onComplete {
            case Success(binding) => logger.info(s"Server started on \${binding.localAddress}")
            case Failure(error) =>
                logger.error(s"Server failed to start: \${error.getMessage}")
                system.terminate()
        }

        sys.addShutdownHook({
            Await.result(system.terminate(), 10.seconds)
            ()
        })
    }
}

object ProductionHelloApi extends HelloApi

object HelloServerApp extends HelloRoutes(ProductionHelloApi) with HelloServer with App {
    start
    ()
}

