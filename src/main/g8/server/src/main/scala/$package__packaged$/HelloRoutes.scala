package $package$

import akka.http.scaladsl.server.Directives._

class HelloRoutes(helloApi: HelloApi) {
    val status = new StatusResource()

    def routes = 
        status.route ~
        helloApi.route
}