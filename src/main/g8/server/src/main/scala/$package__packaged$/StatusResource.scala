package $package$

import akka.http.scaladsl.server.Directives._

class StatusResource {
    val route = path("status") { 
        get { 
            complete { "\"OK\"" } 
        } 
    }
}