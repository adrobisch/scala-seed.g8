package $package$

import org.scalatest._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest

class HelloRoutesSpec extends WordSpec
    with Matchers
    with ScalatestRouteTest {
  val routes: Route = new HelloRoutes(new HelloApi {}).routes
  val helloPath: String = "/hello"      

  s"A GET on \$helloPath" should {
    "return 'hello'" in {
      Get(helloPath) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[String] shouldEqual "hello"
      }
    }
  }
}
