This is a [Giter8][g8] template for an Scala application including:

- Formatting via `Scalariform`
- Packaging via `sbt-native-packager`
- Code Coverage via Scoverage
- Docker packaging 
- Simple akka-http based web server including test
- Common libs for logging and monitoring

Missing:

- docker based integration-test setup
- Kafka Producer and Consumer example

## Usage

```
sbt -Dsbt.version=0.13.16-M1 new https://adrobisch@bitbucket.org/adrobisch/scala-seed.g8
```

## Testing the template

```
sbt g8Test
```

License
-------
Written in 2017 by Andreas Drobisch using the template at https://github.com/scala/scala-seed.g8.

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to
this template to the public domain worldwide. This template is distributed without any warranty.
See <http://creativecommons.org/publicdomain/zero/1.0/>.

[g8]: http://www.foundweekends.org/giter8/
